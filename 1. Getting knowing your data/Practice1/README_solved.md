
# Ex1 - Getting and knowing your Data

### Step 1. Go to https://www.kaggle.com/openfoodfacts/world-food-facts/data

###  Step 2. Download the dataset to your computer and unzip it.


```python
import pandas as pd
import numpy as np
```

### Step 3. Use the tsv file and assign it to a dataframe called food


```python
food = pd.read_csv('https://static.openfoodfacts.org/data/en.openfoodfacts.org.products.csv', sep='\t')
```

    //anaconda/lib/python2.7/site-packages/IPython/core/interactiveshell.py:2717: DtypeWarning: Columns (0,3,5,19,20,24,25,26,27,28,36,37,38,39,48) have mixed types. Specify dtype option on import or set low_memory=False.
      interactivity=interactivity, compiler=compiler, result=result)


### Step 4. See the first 5 entries


```python
food.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>code</th>
      <th>url</th>
      <th>creator</th>
      <th>created_t</th>
      <th>created_datetime</th>
      <th>last_modified_t</th>
      <th>last_modified_datetime</th>
      <th>product_name</th>
      <th>generic_name</th>
      <th>quantity</th>
      <th>...</th>
      <th>fruits-vegetables-nuts_100g</th>
      <th>fruits-vegetables-nuts-estimate_100g</th>
      <th>collagen-meat-protein-ratio_100g</th>
      <th>cocoa_100g</th>
      <th>chlorophyl_100g</th>
      <th>carbon-footprint_100g</th>
      <th>nutrition-score-fr_100g</th>
      <th>nutrition-score-uk_100g</th>
      <th>glycemic-index_100g</th>
      <th>water-hardness_100g</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>3087</td>
      <td>http://world-en.openfoodfacts.org/product/0000...</td>
      <td>openfoodfacts-contributors</td>
      <td>1474103866</td>
      <td>2016-09-17T09:17:46Z</td>
      <td>1474103893</td>
      <td>2016-09-17T09:18:13Z</td>
      <td>Farine de blé noir</td>
      <td>NaN</td>
      <td>1kg</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>4530</td>
      <td>http://world-en.openfoodfacts.org/product/0000...</td>
      <td>usda-ndb-import</td>
      <td>1489069957</td>
      <td>2017-03-09T14:32:37Z</td>
      <td>1489069957</td>
      <td>2017-03-09T14:32:37Z</td>
      <td>Banana Chips Sweetened (Whole)</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>14.0</td>
      <td>14.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4559</td>
      <td>http://world-en.openfoodfacts.org/product/0000...</td>
      <td>usda-ndb-import</td>
      <td>1489069957</td>
      <td>2017-03-09T14:32:37Z</td>
      <td>1489069957</td>
      <td>2017-03-09T14:32:37Z</td>
      <td>Peanuts</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>16087</td>
      <td>http://world-en.openfoodfacts.org/product/0000...</td>
      <td>usda-ndb-import</td>
      <td>1489055731</td>
      <td>2017-03-09T10:35:31Z</td>
      <td>1489055731</td>
      <td>2017-03-09T10:35:31Z</td>
      <td>Organic Salted Nut Mix</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>12.0</td>
      <td>12.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>16094</td>
      <td>http://world-en.openfoodfacts.org/product/0000...</td>
      <td>usda-ndb-import</td>
      <td>1489055653</td>
      <td>2017-03-09T10:34:13Z</td>
      <td>1489055653</td>
      <td>2017-03-09T10:34:13Z</td>
      <td>Organic Polenta</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 163 columns</p>
</div>



### Step 5. What is the number of observations in the dataset?


```python
food.shape #will give you both (observations/rows, columns)
```




    (356027, 163)




```python
food.shape[0] #will give you only the observations/rows number
```




    356027



### Step 6. What is the number of columns in the dataset?


```python
print(food.shape) #will give you both (observations/rows, columns)
print(food.shape[1]) #will give you only the columns number

#OR

food.info() #Columns: 163 entries
```

    (356027, 163)
    163
    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 356027 entries, 0 to 356026
    Columns: 163 entries, code to water-hardness_100g
    dtypes: float64(107), object(56)
    memory usage: 442.8+ MB


### Step 7. Print the name of all the columns.


```python
food.columns
```




    Index([u'code', u'url', u'creator', u'created_t', u'created_datetime',
           u'last_modified_t', u'last_modified_datetime', u'product_name',
           u'generic_name', u'quantity',
           ...
           u'fruits-vegetables-nuts_100g', u'fruits-vegetables-nuts-estimate_100g',
           u'collagen-meat-protein-ratio_100g', u'cocoa_100g', u'chlorophyl_100g',
           u'carbon-footprint_100g', u'nutrition-score-fr_100g',
           u'nutrition-score-uk_100g', u'glycemic-index_100g',
           u'water-hardness_100g'],
          dtype='object', length=163)



### Step 8. What is the name of 105th column?


```python
food.columns[104]
```




    '-glucose_100g'



### Step 9. What is the type of the observations of the 105th column?


```python
food.dtypes['-glucose_100g']
```




    dtype('float64')



### Step 10. How is the dataset indexed?


```python
food.index
```




    RangeIndex(start=0, stop=356027, step=1)



### Step 11. What is the product name of the 19th observation?


```python
food.values[18][7]
```




    'Lotus Organic Brown Jasmine Rice'


