
# EX9 - Regiment

### Step 1. Import the necessary libraries


```python
import pandas as pd
```

### Step 2. Create the DataFrame with the following values:


```python
raw_data = {'regiment': ['Nighthawks', 'Nighthawks', 'Nighthawks', 'Nighthawks', 'Dragoons', 'Dragoons', 'Dragoons', 'Dragoons', 'Scouts', 'Scouts', 'Scouts', 'Scouts'], 
        'company': ['1st', '1st', '2nd', '2nd', '1st', '1st', '2nd', '2nd','1st', '1st', '2nd', '2nd'], 
        'name': ['Miller', 'Jacobson', 'Ali', 'Milner', 'Cooze', 'Jacon', 'Ryaner', 'Sone', 'Sloan', 'Piger', 'Riani', 'Ali'], 
        'preTestScore': [4, 24, 31, 2, 3, 4, 24, 31, 2, 3, 2, 3],
        'postTestScore': [25, 94, 57, 62, 70, 25, 94, 57, 62, 70, 62, 70]}
```

### Step 3. Assign it to a variable called regiment.
#### Don't forget to name each column


```python
regiment = pd.DataFrame(raw_data, columns = raw_data.keys())
regiment
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>name</th>
      <th>preTestScore</th>
      <th>postTestScore</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>Miller</td>
      <td>4</td>
      <td>25</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>Jacobson</td>
      <td>24</td>
      <td>94</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>Ali</td>
      <td>31</td>
      <td>57</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>Milner</td>
      <td>2</td>
      <td>62</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>Cooze</td>
      <td>3</td>
      <td>70</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>Jacon</td>
      <td>4</td>
      <td>25</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>Ryaner</td>
      <td>24</td>
      <td>94</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>Sone</td>
      <td>31</td>
      <td>57</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>Sloan</td>
      <td>2</td>
      <td>62</td>
    </tr>
    <tr>
      <th>9</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>Piger</td>
      <td>3</td>
      <td>70</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>Riani</td>
      <td>2</td>
      <td>62</td>
    </tr>
    <tr>
      <th>11</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>Ali</td>
      <td>3</td>
      <td>70</td>
    </tr>
  </tbody>
</table>
</div>



### Step 4. What is the mean preTestScore from the regiment Nighthawks?  


```python
regiment[regiment['regiment'] == 'Nighthawks'].groupby('regiment').mean()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>preTestScore</th>
      <th>postTestScore</th>
    </tr>
    <tr>
      <th>regiment</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Dragoons</th>
      <td>15.50</td>
      <td>61.5</td>
    </tr>
    <tr>
      <th>Nighthawks</th>
      <td>15.25</td>
      <td>59.5</td>
    </tr>
    <tr>
      <th>Scouts</th>
      <td>2.50</td>
      <td>66.0</td>
    </tr>
  </tbody>
</table>
</div>



### Step 5. Present general statistics by company


```python
regiment.groupby('company').describe()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>postTestScore</th>
      <th>preTestScore</th>
    </tr>
    <tr>
      <th>company</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="8" valign="top">1st</th>
      <th>count</th>
      <td>6.000000</td>
      <td>6.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>57.666667</td>
      <td>6.666667</td>
    </tr>
    <tr>
      <th>std</th>
      <td>27.485754</td>
      <td>8.524475</td>
    </tr>
    <tr>
      <th>min</th>
      <td>25.000000</td>
      <td>2.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>34.250000</td>
      <td>3.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>66.000000</td>
      <td>3.500000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>70.000000</td>
      <td>4.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>94.000000</td>
      <td>24.000000</td>
    </tr>
    <tr>
      <th rowspan="8" valign="top">2nd</th>
      <th>count</th>
      <td>6.000000</td>
      <td>6.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>67.000000</td>
      <td>15.500000</td>
    </tr>
    <tr>
      <th>std</th>
      <td>14.057027</td>
      <td>14.652645</td>
    </tr>
    <tr>
      <th>min</th>
      <td>57.000000</td>
      <td>2.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>58.250000</td>
      <td>2.250000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>62.000000</td>
      <td>13.500000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>68.000000</td>
      <td>29.250000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>94.000000</td>
      <td>31.000000</td>
    </tr>
  </tbody>
</table>
</div>



### Step 6. What is the mean each company's preTestScore?


```python
regiment.groupby('company').preTestScore.mean()
```




    company
    1st     6.666667
    2nd    15.500000
    Name: preTestScore, dtype: float64



### Step 7. Present the mean preTestScores grouped by regiment and company


```python
regiment.groupby(['regiment', 'company']).preTestScore.mean()
```




    regiment    company
    Dragoons    1st         3.5
                2nd        27.5
    Nighthawks  1st        14.0
                2nd        16.5
    Scouts      1st         2.5
                2nd         2.5
    Name: preTestScore, dtype: float64



### Step 8. Present the mean preTestScores grouped by regiment and company without heirarchical indexing


```python
regiment.groupby(['regiment', 'company']).preTestScore.mean().unstack()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>company</th>
      <th>1st</th>
      <th>2nd</th>
    </tr>
    <tr>
      <th>regiment</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Dragoons</th>
      <td>3.5</td>
      <td>27.5</td>
    </tr>
    <tr>
      <th>Nighthawks</th>
      <td>14.0</td>
      <td>16.5</td>
    </tr>
    <tr>
      <th>Scouts</th>
      <td>2.5</td>
      <td>2.5</td>
    </tr>
  </tbody>
</table>
</div>



### Step 9. Group the entire dataframe by regiment and company


```python
regiment.groupby(['regiment', 'company']).mean()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>preTestScore</th>
      <th>postTestScore</th>
    </tr>
    <tr>
      <th>regiment</th>
      <th>company</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="2" valign="top">Dragoons</th>
      <th>1st</th>
      <td>3.5</td>
      <td>47.5</td>
    </tr>
    <tr>
      <th>2nd</th>
      <td>27.5</td>
      <td>75.5</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">Nighthawks</th>
      <th>1st</th>
      <td>14.0</td>
      <td>59.5</td>
    </tr>
    <tr>
      <th>2nd</th>
      <td>16.5</td>
      <td>59.5</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">Scouts</th>
      <th>1st</th>
      <td>2.5</td>
      <td>66.0</td>
    </tr>
    <tr>
      <th>2nd</th>
      <td>2.5</td>
      <td>66.0</td>
    </tr>
  </tbody>
</table>
</div>



### Step 10. What is the number of observations in each regiment and company


```python
regiment.groupby(['company', 'regiment']).size()
```




    company  regiment  
    1st      Dragoons      2
             Nighthawks    2
             Scouts        2
    2nd      Dragoons      2
             Nighthawks    2
             Scouts        2
    dtype: int64



### Step 11. Iterate over a group and print the name and the whole data from the regiment


```python
# Group the dataframe by regiment, and for each regiment,
for name, group in regiment.groupby('regiment'):
    # print the name of the regiment
    print(name)
    # print the data of that regiment
    print(group)
```

    Dragoons
       regiment company    name  preTestScore  postTestScore
    4  Dragoons     1st   Cooze             3             70
    5  Dragoons     1st   Jacon             4             25
    6  Dragoons     2nd  Ryaner            24             94
    7  Dragoons     2nd    Sone            31             57
    Nighthawks
         regiment company      name  preTestScore  postTestScore
    0  Nighthawks     1st    Miller             4             25
    1  Nighthawks     1st  Jacobson            24             94
    2  Nighthawks     2nd       Ali            31             57
    3  Nighthawks     2nd    Milner             2             62
    Scouts
       regiment company   name  preTestScore  postTestScore
    8    Scouts     1st  Sloan             2             62
    9    Scouts     1st  Piger             3             70
    10   Scouts     2nd  Riani             2             62
    11   Scouts     2nd    Ali             3             70

