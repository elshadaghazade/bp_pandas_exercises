
# Fictitious Names

### Introduction:

This time you will create a data again 

In order to understand about it go to [here](https://blog.codinghorror.com/a-visual-explanation-of-sql-joins/).

### Step 1. Import the necessary libraries


```python
import pandas as pd
```

### Step 2. Create the 3 DataFrames based on the followin raw data


```python
raw_data_1 = {
        'subject_id': ['1', '2', '3', '4', '5'],
        'first_name': ['Alex', 'Amy', 'Allen', 'Alice', 'Ayoung'], 
        'last_name': ['Anderson', 'Ackerman', 'Ali', 'Aoni', 'Atiches']}

raw_data_2 = {
        'subject_id': ['4', '5', '6', '7', '8'],
        'first_name': ['Billy', 'Brian', 'Bran', 'Bryce', 'Betty'], 
        'last_name': ['Bonder', 'Black', 'Balwner', 'Brice', 'Btisan']}

raw_data_3 = {
        'subject_id': ['1', '2', '3', '4', '5', '7', '8', '9', '10', '11'],
        'test_id': [51, 15, 15, 61, 16, 14, 15, 1, 61, 16]}
```

### Step 3. Assign each to a variable called data1, data2, data3


```python
data1 = pd.DataFrame(raw_data_1, columns = ['subject_id', 'first_name', 'last_name'])
data2 = pd.DataFrame(raw_data_2, columns = ['subject_id', 'first_name', 'last_name'])
data3 = pd.DataFrame(raw_data_3, columns = ['subject_id','test_id'])

data1 + data2
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>subject_id</th>
      <th>first_name</th>
      <th>last_name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>14</td>
      <td>AlexBilly</td>
      <td>AndersonBonder</td>
    </tr>
    <tr>
      <th>1</th>
      <td>25</td>
      <td>AmyBrian</td>
      <td>AckermanBlack</td>
    </tr>
    <tr>
      <th>2</th>
      <td>36</td>
      <td>AllenBran</td>
      <td>AliBalwner</td>
    </tr>
    <tr>
      <th>3</th>
      <td>47</td>
      <td>AliceBryce</td>
      <td>AoniBrice</td>
    </tr>
    <tr>
      <th>4</th>
      <td>58</td>
      <td>AyoungBetty</td>
      <td>AtichesBtisan</td>
    </tr>
  </tbody>
</table>
</div>



### Step 4. Join the two dataframes along rows and assign all_data

### Step 5. Join the two dataframes along columns and assing to all_data_col


### Step 6. Print data3

### Step 7. Merge all_data and data3 along the subject_id value

### Step 8. Merge only the data that has the same 'subject_id' on both data1 and data2

### Step 9. Merge all values in data1 and data2, with matching records from both sides where available.