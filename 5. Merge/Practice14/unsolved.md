
# Housing Market

### Introduction:

This time we will create our own dataset with fictional numbers to describe a house market. As we are going to create random data don't try to reason of the numbers.

### Step 1. Import the necessary libraries


```python
import pandas as pd
import numpy as np
```

### Step 2. Create 3 differents Series, each of length 100, as follows: 
1. The first a random number from 1 to 4 
2. The second a random number from 1 to 3
3. The third a random number from 10,000 to 30,000


```python
s1 = pd.Series(np.random.randint(1, high=5, size=100, dtype='l'))
s2 = pd.Series(np.random.randint(1, high=4, size=100, dtype='l'))
s3 = pd.Series(np.random.randint(10000, high=30001, size=100, dtype='l'))

print(s1, s2, s3)
```

    0     2
    1     2
    2     4
    3     2
    4     1
    5     1
    6     2
    7     3
    8     3
    9     2
    10    1
    11    2
    12    4
    13    1
    14    2
    15    3
    16    4
    17    4
    18    4
    19    3
    20    2
    21    1
    22    4
    23    1
    24    3
    25    2
    26    3
    27    1
    28    3
    29    4
         ..
    70    4
    71    2
    72    2
    73    4
    74    2
    75    1
    76    2
    77    4
    78    3
    79    2
    80    2
    81    2
    82    4
    83    2
    84    2
    85    2
    86    1
    87    3
    88    1
    89    1
    90    1
    91    3
    92    1
    93    2
    94    3
    95    4
    96    4
    97    2
    98    1
    99    3
    dtype: int64 0     2
    1     3
    2     2
    3     3
    4     3
    5     1
    6     2
    7     1
    8     2
    9     2
    10    2
    11    3
    12    3
    13    1
    14    3
    15    3
    16    3
    17    1
    18    3
    19    3
    20    3
    21    3
    22    1
    23    2
    24    3
    25    2
    26    2
    27    1
    28    3
    29    3
         ..
    70    3
    71    2
    72    2
    73    2
    74    3
    75    2
    76    3
    77    1
    78    1
    79    1
    80    2
    81    1
    82    1
    83    3
    84    1
    85    3
    86    1
    87    2
    88    3
    89    2
    90    2
    91    3
    92    2
    93    2
    94    2
    95    2
    96    2
    97    3
    98    1
    99    1
    dtype: int64 0     16957
    1     24571
    2     28303
    3     14153
    4     23445
    5     21444
    6     16179
    7     22696
    8     18595
    9     27145
    10    14406
    11    15011
    12    17444
    13    26236
    14    23808
    15    21417
    16    15079
    17    13100
    18    21470
    19    17082
    20    21935
    21    26770
    22    10059
    23    11095
    24    25916
    25    17137
    26    22023
    27    21612
    28    11446
    29    29281
          ...  
    70    23963
    71    26782
    72    11199
    73    23600
    74    26935
    75    27365
    76    23084
    77    19052
    78    19922
    79    17088
    80    25468
    81    10924
    82    10243
    83    19834
    84    21288
    85    22410
    86    22348
    87    18812
    88    29522
    89    20838
    90    28695
    91    23000
    92    21684
    93    26316
    94    10866
    95    12337
    96    13480
    97    25158
    98    25585
    99    26142
    dtype: int64


### Step 3. Let's create a DataFrame by joinning the Series by column

### Step 4. Change the name of the columns to bedrs, bathrs, price_sqr_meter

### Step 5. Create a one column DataFrame with the values of the 3 Series and assign it to 'bigcolumn'

### Step 6. Ops it seems it is going only until index 99. Is it true?

### Step 7. Reindex the DataFrame so it goes from 0 to 299