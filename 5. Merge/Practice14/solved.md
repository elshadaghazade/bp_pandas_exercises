
# Housing Market

### Introduction:

This time we will create our own dataset with fictional numbers to describe a house market. As we are going to create random data don't try to reason of the numbers.

### Step 1. Import the necessary libraries


```python
import pandas as pd
import numpy as np
```

### Step 2. Create 3 differents Series, each of length 100, as follows: 
1. The first a random number from 1 to 4 
2. The second a random number from 1 to 3
3. The third a random number from 10,000 to 30,000


```python
s1 = pd.Series(np.random.randint(1, high=5, size=100, dtype='l'))
s2 = pd.Series(np.random.randint(1, high=4, size=100, dtype='l'))
s3 = pd.Series(np.random.randint(10000, high=30001, size=100, dtype='l'))

print(s1, s2, s3)
```

    0     2
    1     2
    2     4
    3     2
    4     1
    5     1
    6     2
    7     3
    8     3
    9     2
    10    1
    11    2
    12    4
    13    1
    14    2
    15    3
    16    4
    17    4
    18    4
    19    3
    20    2
    21    1
    22    4
    23    1
    24    3
    25    2
    26    3
    27    1
    28    3
    29    4
         ..
    70    4
    71    2
    72    2
    73    4
    74    2
    75    1
    76    2
    77    4
    78    3
    79    2
    80    2
    81    2
    82    4
    83    2
    84    2
    85    2
    86    1
    87    3
    88    1
    89    1
    90    1
    91    3
    92    1
    93    2
    94    3
    95    4
    96    4
    97    2
    98    1
    99    3
    dtype: int64 0     2
    1     3
    2     2
    3     3
    4     3
    5     1
    6     2
    7     1
    8     2
    9     2
    10    2
    11    3
    12    3
    13    1
    14    3
    15    3
    16    3
    17    1
    18    3
    19    3
    20    3
    21    3
    22    1
    23    2
    24    3
    25    2
    26    2
    27    1
    28    3
    29    3
         ..
    70    3
    71    2
    72    2
    73    2
    74    3
    75    2
    76    3
    77    1
    78    1
    79    1
    80    2
    81    1
    82    1
    83    3
    84    1
    85    3
    86    1
    87    2
    88    3
    89    2
    90    2
    91    3
    92    2
    93    2
    94    2
    95    2
    96    2
    97    3
    98    1
    99    1
    dtype: int64 0     16957
    1     24571
    2     28303
    3     14153
    4     23445
    5     21444
    6     16179
    7     22696
    8     18595
    9     27145
    10    14406
    11    15011
    12    17444
    13    26236
    14    23808
    15    21417
    16    15079
    17    13100
    18    21470
    19    17082
    20    21935
    21    26770
    22    10059
    23    11095
    24    25916
    25    17137
    26    22023
    27    21612
    28    11446
    29    29281
          ...  
    70    23963
    71    26782
    72    11199
    73    23600
    74    26935
    75    27365
    76    23084
    77    19052
    78    19922
    79    17088
    80    25468
    81    10924
    82    10243
    83    19834
    84    21288
    85    22410
    86    22348
    87    18812
    88    29522
    89    20838
    90    28695
    91    23000
    92    21684
    93    26316
    94    10866
    95    12337
    96    13480
    97    25158
    98    25585
    99    26142
    dtype: int64


### Step 3. Let's create a DataFrame by joinning the Series by column


```python
housemkt = pd.concat([s1, s2, s3], axis=1)
housemkt.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
      <td>2</td>
      <td>16957</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>3</td>
      <td>24571</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>2</td>
      <td>28303</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>3</td>
      <td>14153</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>3</td>
      <td>23445</td>
    </tr>
  </tbody>
</table>
</div>



### Step 4. Change the name of the columns to bedrs, bathrs, price_sqr_meter


```python
housemkt.rename(columns = {0: 'bedrs', 1: 'bathrs', 2: 'price_sqr_meter'}, inplace=True)
housemkt.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bedrs</th>
      <th>bathrs</th>
      <th>price_sqr_meter</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
      <td>2</td>
      <td>16957</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>3</td>
      <td>24571</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>2</td>
      <td>28303</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
      <td>3</td>
      <td>14153</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>3</td>
      <td>23445</td>
    </tr>
  </tbody>
</table>
</div>



### Step 5. Create a one column DataFrame with the values of the 3 Series and assign it to 'bigcolumn'


```python
# join concat the values
bigcolumn = pd.concat([s1, s2, s3], axis=0)

# it is still a Series, so we need to transform it to a DataFrame
bigcolumn = bigcolumn.to_frame()
print(type(bigcolumn))

bigcolumn
```

    <class 'pandas.core.frame.DataFrame'>





<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>2</td>
    </tr>
    <tr>
      <th>7</th>
      <td>3</td>
    </tr>
    <tr>
      <th>8</th>
      <td>3</td>
    </tr>
    <tr>
      <th>9</th>
      <td>2</td>
    </tr>
    <tr>
      <th>10</th>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>2</td>
    </tr>
    <tr>
      <th>12</th>
      <td>4</td>
    </tr>
    <tr>
      <th>13</th>
      <td>1</td>
    </tr>
    <tr>
      <th>14</th>
      <td>2</td>
    </tr>
    <tr>
      <th>15</th>
      <td>3</td>
    </tr>
    <tr>
      <th>16</th>
      <td>4</td>
    </tr>
    <tr>
      <th>17</th>
      <td>4</td>
    </tr>
    <tr>
      <th>18</th>
      <td>4</td>
    </tr>
    <tr>
      <th>19</th>
      <td>3</td>
    </tr>
    <tr>
      <th>20</th>
      <td>2</td>
    </tr>
    <tr>
      <th>21</th>
      <td>1</td>
    </tr>
    <tr>
      <th>22</th>
      <td>4</td>
    </tr>
    <tr>
      <th>23</th>
      <td>1</td>
    </tr>
    <tr>
      <th>24</th>
      <td>3</td>
    </tr>
    <tr>
      <th>25</th>
      <td>2</td>
    </tr>
    <tr>
      <th>26</th>
      <td>3</td>
    </tr>
    <tr>
      <th>27</th>
      <td>1</td>
    </tr>
    <tr>
      <th>28</th>
      <td>3</td>
    </tr>
    <tr>
      <th>29</th>
      <td>4</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>70</th>
      <td>23963</td>
    </tr>
    <tr>
      <th>71</th>
      <td>26782</td>
    </tr>
    <tr>
      <th>72</th>
      <td>11199</td>
    </tr>
    <tr>
      <th>73</th>
      <td>23600</td>
    </tr>
    <tr>
      <th>74</th>
      <td>26935</td>
    </tr>
    <tr>
      <th>75</th>
      <td>27365</td>
    </tr>
    <tr>
      <th>76</th>
      <td>23084</td>
    </tr>
    <tr>
      <th>77</th>
      <td>19052</td>
    </tr>
    <tr>
      <th>78</th>
      <td>19922</td>
    </tr>
    <tr>
      <th>79</th>
      <td>17088</td>
    </tr>
    <tr>
      <th>80</th>
      <td>25468</td>
    </tr>
    <tr>
      <th>81</th>
      <td>10924</td>
    </tr>
    <tr>
      <th>82</th>
      <td>10243</td>
    </tr>
    <tr>
      <th>83</th>
      <td>19834</td>
    </tr>
    <tr>
      <th>84</th>
      <td>21288</td>
    </tr>
    <tr>
      <th>85</th>
      <td>22410</td>
    </tr>
    <tr>
      <th>86</th>
      <td>22348</td>
    </tr>
    <tr>
      <th>87</th>
      <td>18812</td>
    </tr>
    <tr>
      <th>88</th>
      <td>29522</td>
    </tr>
    <tr>
      <th>89</th>
      <td>20838</td>
    </tr>
    <tr>
      <th>90</th>
      <td>28695</td>
    </tr>
    <tr>
      <th>91</th>
      <td>23000</td>
    </tr>
    <tr>
      <th>92</th>
      <td>21684</td>
    </tr>
    <tr>
      <th>93</th>
      <td>26316</td>
    </tr>
    <tr>
      <th>94</th>
      <td>10866</td>
    </tr>
    <tr>
      <th>95</th>
      <td>12337</td>
    </tr>
    <tr>
      <th>96</th>
      <td>13480</td>
    </tr>
    <tr>
      <th>97</th>
      <td>25158</td>
    </tr>
    <tr>
      <th>98</th>
      <td>25585</td>
    </tr>
    <tr>
      <th>99</th>
      <td>26142</td>
    </tr>
  </tbody>
</table>
<p>300 rows × 1 columns</p>
</div>



### Step 6. Ops it seems it is going only until index 99. Is it true?


```python
# no the index are kept but the length of the DataFrame is 300
len(bigcolumn)
```




    300



### Step 7. Reindex the DataFrame so it goes from 0 to 299


```python
bigcolumn.reset_index(drop=True, inplace=True)
bigcolumn
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1</td>
    </tr>
    <tr>
      <th>6</th>
      <td>2</td>
    </tr>
    <tr>
      <th>7</th>
      <td>3</td>
    </tr>
    <tr>
      <th>8</th>
      <td>3</td>
    </tr>
    <tr>
      <th>9</th>
      <td>2</td>
    </tr>
    <tr>
      <th>10</th>
      <td>1</td>
    </tr>
    <tr>
      <th>11</th>
      <td>2</td>
    </tr>
    <tr>
      <th>12</th>
      <td>4</td>
    </tr>
    <tr>
      <th>13</th>
      <td>1</td>
    </tr>
    <tr>
      <th>14</th>
      <td>2</td>
    </tr>
    <tr>
      <th>15</th>
      <td>3</td>
    </tr>
    <tr>
      <th>16</th>
      <td>4</td>
    </tr>
    <tr>
      <th>17</th>
      <td>4</td>
    </tr>
    <tr>
      <th>18</th>
      <td>4</td>
    </tr>
    <tr>
      <th>19</th>
      <td>3</td>
    </tr>
    <tr>
      <th>20</th>
      <td>2</td>
    </tr>
    <tr>
      <th>21</th>
      <td>1</td>
    </tr>
    <tr>
      <th>22</th>
      <td>4</td>
    </tr>
    <tr>
      <th>23</th>
      <td>1</td>
    </tr>
    <tr>
      <th>24</th>
      <td>3</td>
    </tr>
    <tr>
      <th>25</th>
      <td>2</td>
    </tr>
    <tr>
      <th>26</th>
      <td>3</td>
    </tr>
    <tr>
      <th>27</th>
      <td>1</td>
    </tr>
    <tr>
      <th>28</th>
      <td>3</td>
    </tr>
    <tr>
      <th>29</th>
      <td>4</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>270</th>
      <td>23963</td>
    </tr>
    <tr>
      <th>271</th>
      <td>26782</td>
    </tr>
    <tr>
      <th>272</th>
      <td>11199</td>
    </tr>
    <tr>
      <th>273</th>
      <td>23600</td>
    </tr>
    <tr>
      <th>274</th>
      <td>26935</td>
    </tr>
    <tr>
      <th>275</th>
      <td>27365</td>
    </tr>
    <tr>
      <th>276</th>
      <td>23084</td>
    </tr>
    <tr>
      <th>277</th>
      <td>19052</td>
    </tr>
    <tr>
      <th>278</th>
      <td>19922</td>
    </tr>
    <tr>
      <th>279</th>
      <td>17088</td>
    </tr>
    <tr>
      <th>280</th>
      <td>25468</td>
    </tr>
    <tr>
      <th>281</th>
      <td>10924</td>
    </tr>
    <tr>
      <th>282</th>
      <td>10243</td>
    </tr>
    <tr>
      <th>283</th>
      <td>19834</td>
    </tr>
    <tr>
      <th>284</th>
      <td>21288</td>
    </tr>
    <tr>
      <th>285</th>
      <td>22410</td>
    </tr>
    <tr>
      <th>286</th>
      <td>22348</td>
    </tr>
    <tr>
      <th>287</th>
      <td>18812</td>
    </tr>
    <tr>
      <th>288</th>
      <td>29522</td>
    </tr>
    <tr>
      <th>289</th>
      <td>20838</td>
    </tr>
    <tr>
      <th>290</th>
      <td>28695</td>
    </tr>
    <tr>
      <th>291</th>
      <td>23000</td>
    </tr>
    <tr>
      <th>292</th>
      <td>21684</td>
    </tr>
    <tr>
      <th>293</th>
      <td>26316</td>
    </tr>
    <tr>
      <th>294</th>
      <td>10866</td>
    </tr>
    <tr>
      <th>295</th>
      <td>12337</td>
    </tr>
    <tr>
      <th>296</th>
      <td>13480</td>
    </tr>
    <tr>
      <th>297</th>
      <td>25158</td>
    </tr>
    <tr>
      <th>298</th>
      <td>25585</td>
    </tr>
    <tr>
      <th>299</th>
      <td>26142</td>
    </tr>
  </tbody>
</table>
<p>300 rows × 1 columns</p>
</div>


