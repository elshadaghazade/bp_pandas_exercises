
# MPG Cars

### Introduction:

The following exercise utilizes data from [UC Irvine Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Auto+MPG)

### Step 1. Import the necessary libraries


```python
import pandas as pd
import numpy as np
```

### Step 2. Import the first dataset [cars1](https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars1.csv) and [cars2](https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars2.csv).  

   ### Step 3. Assign each to a to a variable called cars1 and cars2


```python
cars1 = pd.read_csv("https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars1.csv")
cars2 = pd.read_csv("https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars2.csv")

print(cars1.head())
print(cars2.head())
```

        mpg  cylinders  displacement horsepower  weight  acceleration  model  \
    0  18.0          8           307        130    3504          12.0     70   
    1  15.0          8           350        165    3693          11.5     70   
    2  18.0          8           318        150    3436          11.0     70   
    3  16.0          8           304        150    3433          12.0     70   
    4  17.0          8           302        140    3449          10.5     70   
    
       origin                        car  Unnamed: 9  Unnamed: 10  Unnamed: 11  \
    0       1  chevrolet chevelle malibu         NaN          NaN          NaN   
    1       1          buick skylark 320         NaN          NaN          NaN   
    2       1         plymouth satellite         NaN          NaN          NaN   
    3       1              amc rebel sst         NaN          NaN          NaN   
    4       1                ford torino         NaN          NaN          NaN   
    
       Unnamed: 12  Unnamed: 13  
    0          NaN          NaN  
    1          NaN          NaN  
    2          NaN          NaN  
    3          NaN          NaN  
    4          NaN          NaN  
        mpg  cylinders  displacement horsepower  weight  acceleration  model  \
    0  33.0          4            91         53    1795          17.4     76   
    1  20.0          6           225        100    3651          17.7     76   
    2  18.0          6           250         78    3574          21.0     76   
    3  18.5          6           250        110    3645          16.2     76   
    4  17.5          6           258         95    3193          17.8     76   
    
       origin                 car  
    0       3         honda civic  
    1       1      dodge aspen se  
    2       1   ford granada ghia  
    3       1  pontiac ventura sj  
    4       1       amc pacer d/l  


### Step 4. Ops it seems our first dataset has some unnamed blank columns, fix cars1

### Step 5. What is the number of observations in each dataset?

### Step 6. Join cars1 and cars2 into a single DataFrame called cars

### Step 7. Ops there is a column missing, called owners. Create a random number Series from 15,000 to 73,000.

### Step 8. Add the column owners to cars