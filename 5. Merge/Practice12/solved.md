
# MPG Cars

### Introduction:

The following exercise utilizes data from [UC Irvine Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Auto+MPG)

### Step 1. Import the necessary libraries


```python
import pandas as pd
import numpy as np
```

### Step 2. Import the first dataset [cars1](https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars1.csv) and [cars2](https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars2.csv).  

   ### Step 3. Assign each to a to a variable called cars1 and cars2


```python
cars1 = pd.read_csv("https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars1.csv")
cars2 = pd.read_csv("https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/05_Merge/Auto_MPG/cars2.csv")

print(cars1.head())
print(cars2.head())
```

        mpg  cylinders  displacement horsepower  weight  acceleration  model  \
    0  18.0          8           307        130    3504          12.0     70   
    1  15.0          8           350        165    3693          11.5     70   
    2  18.0          8           318        150    3436          11.0     70   
    3  16.0          8           304        150    3433          12.0     70   
    4  17.0          8           302        140    3449          10.5     70   
    
       origin                        car  Unnamed: 9  Unnamed: 10  Unnamed: 11  \
    0       1  chevrolet chevelle malibu         NaN          NaN          NaN   
    1       1          buick skylark 320         NaN          NaN          NaN   
    2       1         plymouth satellite         NaN          NaN          NaN   
    3       1              amc rebel sst         NaN          NaN          NaN   
    4       1                ford torino         NaN          NaN          NaN   
    
       Unnamed: 12  Unnamed: 13  
    0          NaN          NaN  
    1          NaN          NaN  
    2          NaN          NaN  
    3          NaN          NaN  
    4          NaN          NaN  
        mpg  cylinders  displacement horsepower  weight  acceleration  model  \
    0  33.0          4            91         53    1795          17.4     76   
    1  20.0          6           225        100    3651          17.7     76   
    2  18.0          6           250         78    3574          21.0     76   
    3  18.5          6           250        110    3645          16.2     76   
    4  17.5          6           258         95    3193          17.8     76   
    
       origin                 car  
    0       3         honda civic  
    1       1      dodge aspen se  
    2       1   ford granada ghia  
    3       1  pontiac ventura sj  
    4       1       amc pacer d/l  


### Step 4. Ops it seems our first dataset has some unnamed blank columns, fix cars1


```python
cars1 = cars1.loc[:, "mpg":"car"]
cars1.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mpg</th>
      <th>cylinders</th>
      <th>displacement</th>
      <th>horsepower</th>
      <th>weight</th>
      <th>acceleration</th>
      <th>model</th>
      <th>origin</th>
      <th>car</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>18.0</td>
      <td>8</td>
      <td>307</td>
      <td>130</td>
      <td>3504</td>
      <td>12.0</td>
      <td>70</td>
      <td>1</td>
      <td>chevrolet chevelle malibu</td>
    </tr>
    <tr>
      <th>1</th>
      <td>15.0</td>
      <td>8</td>
      <td>350</td>
      <td>165</td>
      <td>3693</td>
      <td>11.5</td>
      <td>70</td>
      <td>1</td>
      <td>buick skylark 320</td>
    </tr>
    <tr>
      <th>2</th>
      <td>18.0</td>
      <td>8</td>
      <td>318</td>
      <td>150</td>
      <td>3436</td>
      <td>11.0</td>
      <td>70</td>
      <td>1</td>
      <td>plymouth satellite</td>
    </tr>
    <tr>
      <th>3</th>
      <td>16.0</td>
      <td>8</td>
      <td>304</td>
      <td>150</td>
      <td>3433</td>
      <td>12.0</td>
      <td>70</td>
      <td>1</td>
      <td>amc rebel sst</td>
    </tr>
    <tr>
      <th>4</th>
      <td>17.0</td>
      <td>8</td>
      <td>302</td>
      <td>140</td>
      <td>3449</td>
      <td>10.5</td>
      <td>70</td>
      <td>1</td>
      <td>ford torino</td>
    </tr>
  </tbody>
</table>
</div>



### Step 5. What is the number of observations in each dataset?


```python
print(cars1.shape)
print(cars2.shape)
```

    (198, 9)
    (200, 9)


### Step 6. Join cars1 and cars2 into a single DataFrame called cars


```python
cars = cars1.append(cars2)
cars
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mpg</th>
      <th>cylinders</th>
      <th>displacement</th>
      <th>horsepower</th>
      <th>weight</th>
      <th>acceleration</th>
      <th>model</th>
      <th>origin</th>
      <th>car</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>18.0</td>
      <td>8</td>
      <td>307</td>
      <td>130</td>
      <td>3504</td>
      <td>12.0</td>
      <td>70</td>
      <td>1</td>
      <td>chevrolet chevelle malibu</td>
    </tr>
    <tr>
      <th>1</th>
      <td>15.0</td>
      <td>8</td>
      <td>350</td>
      <td>165</td>
      <td>3693</td>
      <td>11.5</td>
      <td>70</td>
      <td>1</td>
      <td>buick skylark 320</td>
    </tr>
    <tr>
      <th>2</th>
      <td>18.0</td>
      <td>8</td>
      <td>318</td>
      <td>150</td>
      <td>3436</td>
      <td>11.0</td>
      <td>70</td>
      <td>1</td>
      <td>plymouth satellite</td>
    </tr>
    <tr>
      <th>3</th>
      <td>16.0</td>
      <td>8</td>
      <td>304</td>
      <td>150</td>
      <td>3433</td>
      <td>12.0</td>
      <td>70</td>
      <td>1</td>
      <td>amc rebel sst</td>
    </tr>
    <tr>
      <th>4</th>
      <td>17.0</td>
      <td>8</td>
      <td>302</td>
      <td>140</td>
      <td>3449</td>
      <td>10.5</td>
      <td>70</td>
      <td>1</td>
      <td>ford torino</td>
    </tr>
    <tr>
      <th>5</th>
      <td>15.0</td>
      <td>8</td>
      <td>429</td>
      <td>198</td>
      <td>4341</td>
      <td>10.0</td>
      <td>70</td>
      <td>1</td>
      <td>ford galaxie 500</td>
    </tr>
    <tr>
      <th>6</th>
      <td>14.0</td>
      <td>8</td>
      <td>454</td>
      <td>220</td>
      <td>4354</td>
      <td>9.0</td>
      <td>70</td>
      <td>1</td>
      <td>chevrolet impala</td>
    </tr>
    <tr>
      <th>7</th>
      <td>14.0</td>
      <td>8</td>
      <td>440</td>
      <td>215</td>
      <td>4312</td>
      <td>8.5</td>
      <td>70</td>
      <td>1</td>
      <td>plymouth fury iii</td>
    </tr>
    <tr>
      <th>8</th>
      <td>14.0</td>
      <td>8</td>
      <td>455</td>
      <td>225</td>
      <td>4425</td>
      <td>10.0</td>
      <td>70</td>
      <td>1</td>
      <td>pontiac catalina</td>
    </tr>
    <tr>
      <th>9</th>
      <td>15.0</td>
      <td>8</td>
      <td>390</td>
      <td>190</td>
      <td>3850</td>
      <td>8.5</td>
      <td>70</td>
      <td>1</td>
      <td>amc ambassador dpl</td>
    </tr>
    <tr>
      <th>10</th>
      <td>15.0</td>
      <td>8</td>
      <td>383</td>
      <td>170</td>
      <td>3563</td>
      <td>10.0</td>
      <td>70</td>
      <td>1</td>
      <td>dodge challenger se</td>
    </tr>
    <tr>
      <th>11</th>
      <td>14.0</td>
      <td>8</td>
      <td>340</td>
      <td>160</td>
      <td>3609</td>
      <td>8.0</td>
      <td>70</td>
      <td>1</td>
      <td>plymouth 'cuda 340</td>
    </tr>
    <tr>
      <th>12</th>
      <td>15.0</td>
      <td>8</td>
      <td>400</td>
      <td>150</td>
      <td>3761</td>
      <td>9.5</td>
      <td>70</td>
      <td>1</td>
      <td>chevrolet monte carlo</td>
    </tr>
    <tr>
      <th>13</th>
      <td>14.0</td>
      <td>8</td>
      <td>455</td>
      <td>225</td>
      <td>3086</td>
      <td>10.0</td>
      <td>70</td>
      <td>1</td>
      <td>buick estate wagon (sw)</td>
    </tr>
    <tr>
      <th>14</th>
      <td>24.0</td>
      <td>4</td>
      <td>113</td>
      <td>95</td>
      <td>2372</td>
      <td>15.0</td>
      <td>70</td>
      <td>3</td>
      <td>toyota corona mark ii</td>
    </tr>
    <tr>
      <th>15</th>
      <td>22.0</td>
      <td>6</td>
      <td>198</td>
      <td>95</td>
      <td>2833</td>
      <td>15.5</td>
      <td>70</td>
      <td>1</td>
      <td>plymouth duster</td>
    </tr>
    <tr>
      <th>16</th>
      <td>18.0</td>
      <td>6</td>
      <td>199</td>
      <td>97</td>
      <td>2774</td>
      <td>15.5</td>
      <td>70</td>
      <td>1</td>
      <td>amc hornet</td>
    </tr>
    <tr>
      <th>17</th>
      <td>21.0</td>
      <td>6</td>
      <td>200</td>
      <td>85</td>
      <td>2587</td>
      <td>16.0</td>
      <td>70</td>
      <td>1</td>
      <td>ford maverick</td>
    </tr>
    <tr>
      <th>18</th>
      <td>27.0</td>
      <td>4</td>
      <td>97</td>
      <td>88</td>
      <td>2130</td>
      <td>14.5</td>
      <td>70</td>
      <td>3</td>
      <td>datsun pl510</td>
    </tr>
    <tr>
      <th>19</th>
      <td>26.0</td>
      <td>4</td>
      <td>97</td>
      <td>46</td>
      <td>1835</td>
      <td>20.5</td>
      <td>70</td>
      <td>2</td>
      <td>volkswagen 1131 deluxe sedan</td>
    </tr>
    <tr>
      <th>20</th>
      <td>25.0</td>
      <td>4</td>
      <td>110</td>
      <td>87</td>
      <td>2672</td>
      <td>17.5</td>
      <td>70</td>
      <td>2</td>
      <td>peugeot 504</td>
    </tr>
    <tr>
      <th>21</th>
      <td>24.0</td>
      <td>4</td>
      <td>107</td>
      <td>90</td>
      <td>2430</td>
      <td>14.5</td>
      <td>70</td>
      <td>2</td>
      <td>audi 100 ls</td>
    </tr>
    <tr>
      <th>22</th>
      <td>25.0</td>
      <td>4</td>
      <td>104</td>
      <td>95</td>
      <td>2375</td>
      <td>17.5</td>
      <td>70</td>
      <td>2</td>
      <td>saab 99e</td>
    </tr>
    <tr>
      <th>23</th>
      <td>26.0</td>
      <td>4</td>
      <td>121</td>
      <td>113</td>
      <td>2234</td>
      <td>12.5</td>
      <td>70</td>
      <td>2</td>
      <td>bmw 2002</td>
    </tr>
    <tr>
      <th>24</th>
      <td>21.0</td>
      <td>6</td>
      <td>199</td>
      <td>90</td>
      <td>2648</td>
      <td>15.0</td>
      <td>70</td>
      <td>1</td>
      <td>amc gremlin</td>
    </tr>
    <tr>
      <th>25</th>
      <td>10.0</td>
      <td>8</td>
      <td>360</td>
      <td>215</td>
      <td>4615</td>
      <td>14.0</td>
      <td>70</td>
      <td>1</td>
      <td>ford f250</td>
    </tr>
    <tr>
      <th>26</th>
      <td>10.0</td>
      <td>8</td>
      <td>307</td>
      <td>200</td>
      <td>4376</td>
      <td>15.0</td>
      <td>70</td>
      <td>1</td>
      <td>chevy c20</td>
    </tr>
    <tr>
      <th>27</th>
      <td>11.0</td>
      <td>8</td>
      <td>318</td>
      <td>210</td>
      <td>4382</td>
      <td>13.5</td>
      <td>70</td>
      <td>1</td>
      <td>dodge d200</td>
    </tr>
    <tr>
      <th>28</th>
      <td>9.0</td>
      <td>8</td>
      <td>304</td>
      <td>193</td>
      <td>4732</td>
      <td>18.5</td>
      <td>70</td>
      <td>1</td>
      <td>hi 1200d</td>
    </tr>
    <tr>
      <th>29</th>
      <td>27.0</td>
      <td>4</td>
      <td>97</td>
      <td>88</td>
      <td>2130</td>
      <td>14.5</td>
      <td>71</td>
      <td>3</td>
      <td>datsun pl510</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>170</th>
      <td>27.0</td>
      <td>4</td>
      <td>112</td>
      <td>88</td>
      <td>2640</td>
      <td>18.6</td>
      <td>82</td>
      <td>1</td>
      <td>chevrolet cavalier wagon</td>
    </tr>
    <tr>
      <th>171</th>
      <td>34.0</td>
      <td>4</td>
      <td>112</td>
      <td>88</td>
      <td>2395</td>
      <td>18.0</td>
      <td>82</td>
      <td>1</td>
      <td>chevrolet cavalier 2-door</td>
    </tr>
    <tr>
      <th>172</th>
      <td>31.0</td>
      <td>4</td>
      <td>112</td>
      <td>85</td>
      <td>2575</td>
      <td>16.2</td>
      <td>82</td>
      <td>1</td>
      <td>pontiac j2000 se hatchback</td>
    </tr>
    <tr>
      <th>173</th>
      <td>29.0</td>
      <td>4</td>
      <td>135</td>
      <td>84</td>
      <td>2525</td>
      <td>16.0</td>
      <td>82</td>
      <td>1</td>
      <td>dodge aries se</td>
    </tr>
    <tr>
      <th>174</th>
      <td>27.0</td>
      <td>4</td>
      <td>151</td>
      <td>90</td>
      <td>2735</td>
      <td>18.0</td>
      <td>82</td>
      <td>1</td>
      <td>pontiac phoenix</td>
    </tr>
    <tr>
      <th>175</th>
      <td>24.0</td>
      <td>4</td>
      <td>140</td>
      <td>92</td>
      <td>2865</td>
      <td>16.4</td>
      <td>82</td>
      <td>1</td>
      <td>ford fairmont futura</td>
    </tr>
    <tr>
      <th>176</th>
      <td>23.0</td>
      <td>4</td>
      <td>151</td>
      <td>?</td>
      <td>3035</td>
      <td>20.5</td>
      <td>82</td>
      <td>1</td>
      <td>amc concord dl</td>
    </tr>
    <tr>
      <th>177</th>
      <td>36.0</td>
      <td>4</td>
      <td>105</td>
      <td>74</td>
      <td>1980</td>
      <td>15.3</td>
      <td>82</td>
      <td>2</td>
      <td>volkswagen rabbit l</td>
    </tr>
    <tr>
      <th>178</th>
      <td>37.0</td>
      <td>4</td>
      <td>91</td>
      <td>68</td>
      <td>2025</td>
      <td>18.2</td>
      <td>82</td>
      <td>3</td>
      <td>mazda glc custom l</td>
    </tr>
    <tr>
      <th>179</th>
      <td>31.0</td>
      <td>4</td>
      <td>91</td>
      <td>68</td>
      <td>1970</td>
      <td>17.6</td>
      <td>82</td>
      <td>3</td>
      <td>mazda glc custom</td>
    </tr>
    <tr>
      <th>180</th>
      <td>38.0</td>
      <td>4</td>
      <td>105</td>
      <td>63</td>
      <td>2125</td>
      <td>14.7</td>
      <td>82</td>
      <td>1</td>
      <td>plymouth horizon miser</td>
    </tr>
    <tr>
      <th>181</th>
      <td>36.0</td>
      <td>4</td>
      <td>98</td>
      <td>70</td>
      <td>2125</td>
      <td>17.3</td>
      <td>82</td>
      <td>1</td>
      <td>mercury lynx l</td>
    </tr>
    <tr>
      <th>182</th>
      <td>36.0</td>
      <td>4</td>
      <td>120</td>
      <td>88</td>
      <td>2160</td>
      <td>14.5</td>
      <td>82</td>
      <td>3</td>
      <td>nissan stanza xe</td>
    </tr>
    <tr>
      <th>183</th>
      <td>36.0</td>
      <td>4</td>
      <td>107</td>
      <td>75</td>
      <td>2205</td>
      <td>14.5</td>
      <td>82</td>
      <td>3</td>
      <td>honda accord</td>
    </tr>
    <tr>
      <th>184</th>
      <td>34.0</td>
      <td>4</td>
      <td>108</td>
      <td>70</td>
      <td>2245</td>
      <td>16.9</td>
      <td>82</td>
      <td>3</td>
      <td>toyota corolla</td>
    </tr>
    <tr>
      <th>185</th>
      <td>38.0</td>
      <td>4</td>
      <td>91</td>
      <td>67</td>
      <td>1965</td>
      <td>15.0</td>
      <td>82</td>
      <td>3</td>
      <td>honda civic</td>
    </tr>
    <tr>
      <th>186</th>
      <td>32.0</td>
      <td>4</td>
      <td>91</td>
      <td>67</td>
      <td>1965</td>
      <td>15.7</td>
      <td>82</td>
      <td>3</td>
      <td>honda civic (auto)</td>
    </tr>
    <tr>
      <th>187</th>
      <td>38.0</td>
      <td>4</td>
      <td>91</td>
      <td>67</td>
      <td>1995</td>
      <td>16.2</td>
      <td>82</td>
      <td>3</td>
      <td>datsun 310 gx</td>
    </tr>
    <tr>
      <th>188</th>
      <td>25.0</td>
      <td>6</td>
      <td>181</td>
      <td>110</td>
      <td>2945</td>
      <td>16.4</td>
      <td>82</td>
      <td>1</td>
      <td>buick century limited</td>
    </tr>
    <tr>
      <th>189</th>
      <td>38.0</td>
      <td>6</td>
      <td>262</td>
      <td>85</td>
      <td>3015</td>
      <td>17.0</td>
      <td>82</td>
      <td>1</td>
      <td>oldsmobile cutlass ciera (diesel)</td>
    </tr>
    <tr>
      <th>190</th>
      <td>26.0</td>
      <td>4</td>
      <td>156</td>
      <td>92</td>
      <td>2585</td>
      <td>14.5</td>
      <td>82</td>
      <td>1</td>
      <td>chrysler lebaron medallion</td>
    </tr>
    <tr>
      <th>191</th>
      <td>22.0</td>
      <td>6</td>
      <td>232</td>
      <td>112</td>
      <td>2835</td>
      <td>14.7</td>
      <td>82</td>
      <td>1</td>
      <td>ford granada l</td>
    </tr>
    <tr>
      <th>192</th>
      <td>32.0</td>
      <td>4</td>
      <td>144</td>
      <td>96</td>
      <td>2665</td>
      <td>13.9</td>
      <td>82</td>
      <td>3</td>
      <td>toyota celica gt</td>
    </tr>
    <tr>
      <th>193</th>
      <td>36.0</td>
      <td>4</td>
      <td>135</td>
      <td>84</td>
      <td>2370</td>
      <td>13.0</td>
      <td>82</td>
      <td>1</td>
      <td>dodge charger 2.2</td>
    </tr>
    <tr>
      <th>194</th>
      <td>27.0</td>
      <td>4</td>
      <td>151</td>
      <td>90</td>
      <td>2950</td>
      <td>17.3</td>
      <td>82</td>
      <td>1</td>
      <td>chevrolet camaro</td>
    </tr>
    <tr>
      <th>195</th>
      <td>27.0</td>
      <td>4</td>
      <td>140</td>
      <td>86</td>
      <td>2790</td>
      <td>15.6</td>
      <td>82</td>
      <td>1</td>
      <td>ford mustang gl</td>
    </tr>
    <tr>
      <th>196</th>
      <td>44.0</td>
      <td>4</td>
      <td>97</td>
      <td>52</td>
      <td>2130</td>
      <td>24.6</td>
      <td>82</td>
      <td>2</td>
      <td>vw pickup</td>
    </tr>
    <tr>
      <th>197</th>
      <td>32.0</td>
      <td>4</td>
      <td>135</td>
      <td>84</td>
      <td>2295</td>
      <td>11.6</td>
      <td>82</td>
      <td>1</td>
      <td>dodge rampage</td>
    </tr>
    <tr>
      <th>198</th>
      <td>28.0</td>
      <td>4</td>
      <td>120</td>
      <td>79</td>
      <td>2625</td>
      <td>18.6</td>
      <td>82</td>
      <td>1</td>
      <td>ford ranger</td>
    </tr>
    <tr>
      <th>199</th>
      <td>31.0</td>
      <td>4</td>
      <td>119</td>
      <td>82</td>
      <td>2720</td>
      <td>19.4</td>
      <td>82</td>
      <td>1</td>
      <td>chevy s-10</td>
    </tr>
  </tbody>
</table>
<p>398 rows × 9 columns</p>
</div>



### Step 7. Ops there is a column missing, called owners. Create a random number Series from 15,000 to 73,000.


```python
nr_owners = np.random.randint(15000, high=73001, size=398, dtype='l')
nr_owners
```




    array([29487, 25680, 65268, 31827, 69215, 72602, 52693, 58440, 16183,
           45014, 32318, 72942, 62163, 35951, 57625, 59355, 36533, 67048,
           58159, 69743, 25146, 22755, 44966, 46792, 56553, 65013, 55908,
           69563, 22030, 59561, 15593, 52998, 54795, 16169, 24809, 35580,
           46590, 38792, 43099, 37166, 21390, 56496, 68606, 21110, 56334,
           45477, 51961, 27625, 51176, 30796, 61809, 65450, 67375, 23342,
           27499, 50585, 57302, 56191, 60281, 32865, 58605, 66374, 15315,
           31791, 28670, 38796, 69214, 41055, 32353, 31574, 65799, 42998,
           72785, 18415, 31977, 29812, 65439, 21161, 60871, 67151, 22179,
           32821, 55392, 34586, 67937, 31646, 66397, 35258, 63815, 71291,
           51130, 27684, 49648, 52691, 50681, 68185, 32635, 51553, 28970,
           19112, 26035, 67666, 55471, 51477, 62055, 53003, 41265, 18565,
           48851, 48673, 45832, 67891, 57638, 29240, 41236, 16950, 31449,
           50528, 22397, 15876, 26414, 16736, 23896, 46104, 17583, 65951,
           38538, 31443, 19299, 46095, 31239, 19290, 38051, 68575, 61755,
           22560, 34460, 35395, 34608, 56906, 44895, 48429, 20900, 49770,
           50513, 59402, 26893, 37233, 19036, 20523, 18765, 46333, 42831,
           53698, 25218, 63106, 16928, 34901, 43674, 65453, 54428, 68502,
           19043, 20325, 45039, 29466, 49672, 67972, 30547, 22522, 69354,
           40489, 72887, 15724, 51442, 65182, 64555, 42138, 72988, 20861,
           67898, 20768, 36415, 47480, 16820, 48739, 62610, 43473, 23002,
           43488, 62581, 37724, 63019, 44912, 35595, 59188, 51814, 65283,
           53479, 27660, 38237, 22957, 47870, 15533, 41944, 51830, 56676,
           57481, 48529, 72220, 66675, 50099, 30585, 25436, 49195, 26050,
           24899, 37213, 25870, 67447, 23808, 71275, 67572, 18545, 43553,
           54858, 23077, 33705, 31282, 26298, 23742, 36110, 51491, 18019,
           60655, 27453, 35563, 63627, 35315, 56717, 59281, 55634, 18415,
           59570, 47320, 20110, 18425, 19352, 18032, 31816, 28573, 66030,
           54723, 21592, 37160, 59518, 35629, 47619, 52359, 34566, 64932,
           24072, 39445, 31203, 63975, 62041, 70175, 51029, 32058, 19428,
           65553, 50799, 48190, 68061, 68201, 53389, 15901, 44585, 54723,
           30446, 63716, 57488, 67134, 22033, 53694, 40002, 24854, 59747,
           59827, 53378, 53196, 68686, 20784, 28181, 33044, 41694, 39857,
           57296, 69021, 17359, 29794, 22515, 55877, 22806, 50027, 56787,
           50844, 17420, 65259, 19141, 40204, 19530, 30116, 34973, 15641,
           53492, 59574, 59082, 64400, 70163, 43058, 69696, 67996, 26158,
           32936, 45461, 47390, 32368, 15400, 40895, 16572, 31776, 62121,
           56704, 39335, 27716, 52565, 50831, 45049, 25173, 25018, 18606,
           71177, 66288, 46754, 68175, 35829, 24959, 54792, 19059, 29092,
           58736, 62938, 44733, 17884, 33905, 33965, 24641, 52257, 28178,
           29515, 37703, 56036, 51556, 23590, 61888, 70224, 53730, 41328,
           16501, 30360, 54106, 29101, 35631, 56173, 30424, 46887, 23657,
           17723, 71709, 45270, 30380, 27779, 33774, 36379, 47127, 63625,
           16750, 65740, 53802, 40995, 37487, 42791, 21825, 69344, 63210,
           15982, 20259])



### Step 8. Add the column owners to cars


```python
cars['owners'] = nr_owners
cars.tail()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mpg</th>
      <th>cylinders</th>
      <th>displacement</th>
      <th>horsepower</th>
      <th>weight</th>
      <th>acceleration</th>
      <th>model</th>
      <th>origin</th>
      <th>car</th>
      <th>owners</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>195</th>
      <td>27.0</td>
      <td>4</td>
      <td>140</td>
      <td>86</td>
      <td>2790</td>
      <td>15.6</td>
      <td>82</td>
      <td>1</td>
      <td>ford mustang gl</td>
      <td>21825</td>
    </tr>
    <tr>
      <th>196</th>
      <td>44.0</td>
      <td>4</td>
      <td>97</td>
      <td>52</td>
      <td>2130</td>
      <td>24.6</td>
      <td>82</td>
      <td>2</td>
      <td>vw pickup</td>
      <td>69344</td>
    </tr>
    <tr>
      <th>197</th>
      <td>32.0</td>
      <td>4</td>
      <td>135</td>
      <td>84</td>
      <td>2295</td>
      <td>11.6</td>
      <td>82</td>
      <td>1</td>
      <td>dodge rampage</td>
      <td>63210</td>
    </tr>
    <tr>
      <th>198</th>
      <td>28.0</td>
      <td>4</td>
      <td>120</td>
      <td>79</td>
      <td>2625</td>
      <td>18.6</td>
      <td>82</td>
      <td>1</td>
      <td>ford ranger</td>
      <td>15982</td>
    </tr>
    <tr>
      <th>199</th>
      <td>31.0</td>
      <td>4</td>
      <td>119</td>
      <td>82</td>
      <td>2720</td>
      <td>19.4</td>
      <td>82</td>
      <td>1</td>
      <td>chevy s-10</td>
      <td>20259</td>
    </tr>
  </tbody>
</table>
</div>


