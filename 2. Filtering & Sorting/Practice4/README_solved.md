
# Ex4 - Filtering and Sorting Data

This time we are going to pull data directly from the internet: https://github.com/justmarkham

### Step 1. Import the necessary libraries


```python
import pandas as pd
```

### Step 2. Import the dataset from this [address](https://raw.githubusercontent.com/justmarkham/DAT8/master/data/chipotle.tsv). 

### Step 3. Assign it to a variable called chipo.


```python
url = 'https://raw.githubusercontent.com/justmarkham/DAT8/master/data/chipotle.tsv'

chipo = pd.read_csv(url, sep = '\t')
```

### Step 4. How many products cost more than $10.00?


```python
# clean the item_price column and transform it in a float
prices = [float(value[1 : -1]) for value in chipo.item_price]

# reassign the column with the cleaned prices
chipo.item_price = prices

# delete the duplicates in item_name and quantity
chipo_filtered = chipo.drop_duplicates(['item_name','quantity'])

# select only the products with quantity equals to 1
chipo_one_prod = chipo_filtered[chipo_filtered.quantity == 1]

chipo_one_prod[chipo_one_prod['item_price']>10].item_name.nunique()
```

### Step 5. What is the price of each item? 
###### print a data frame with only two columns item_name and item_price


```python
# delete the duplicates in item_name and quantity
# chipo_filtered = chipo.drop_duplicates(['item_name','quantity'])
chipo[(chipo['item_name'] == 'Chicken Bowl') & (chipo['quantity'] == 1)]

# select only the products with quantity equals to 1
# chipo_one_prod = chipo_filtered[chipo_filtered.quantity == 1]

# select only the item_name and item_price columns
# price_per_item = chipo_one_prod[['item_name', 'item_price']]

# sort the values from the most to less expensive
# price_per_item.sort_values(by = "item_price", ascending = False).head(20)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>order_id</th>
      <th>quantity</th>
      <th>item_name</th>
      <th>choice_description</th>
      <th>item_price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>3</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa (Mild), [Rice, Cheese, Sou...</td>
      <td>10.98</td>
    </tr>
    <tr>
      <th>13</th>
      <td>7</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>19</th>
      <td>10</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Fajita Vegetables...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>26</th>
      <td>13</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa (Medium), [Pinto Bea...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>42</th>
      <td>20</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Rice, Black Beans,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>76</th>
      <td>34</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Pinto...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>78</th>
      <td>34</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>99</th>
      <td>44</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Rice, Fajita Vege...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>110</th>
      <td>49</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Rice, Black Beans...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>123</th>
      <td>54</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Guacamole, Cheese, Sour ...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>138</th>
      <td>62</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Fajita Vegetables,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>140</th>
      <td>63</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Sour Crea...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>142</th>
      <td>64</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>160</th>
      <td>73</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa (Mild), [Black Beans, Rice...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>176</th>
      <td>79</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa (Medium), [Black Bea...</td>
      <td>10.98</td>
    </tr>
    <tr>
      <th>182</th>
      <td>82</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Sour ...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>193</th>
      <td>86</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream]]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>199</th>
      <td>89</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[[Roasted Chili Corn Salsa (Medium), Tomatillo...</td>
      <td>10.98</td>
    </tr>
    <tr>
      <th>206</th>
      <td>92</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Cheese, Lettuce]]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>209</th>
      <td>93</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Fajita Vegetables,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>211</th>
      <td>93</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Fajita Vegetables,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>218</th>
      <td>96</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Black Bea...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>220</th>
      <td>97</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Rice, Black Beans,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>224</th>
      <td>98</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Pinto Bea...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>231</th>
      <td>102</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>252</th>
      <td>110</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Rice, Cheese, Let...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>257</th>
      <td>111</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Cheese]]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>284</th>
      <td>124</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream]]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>290</th>
      <td>126</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Fajita Vegetables,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>291</th>
      <td>127</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Sour Crea...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>4443</th>
      <td>1771</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Pinto Bea...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4444</th>
      <td>1771</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Sour ...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4465</th>
      <td>1779</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Cheese]]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4481</th>
      <td>1786</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, Rice]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4483</th>
      <td>1786</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Pinto Beans, Chees...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4484</th>
      <td>1786</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4486</th>
      <td>1786</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4488</th>
      <td>1786</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4495</th>
      <td>1788</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Pinto Beans, Sour ...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4498</th>
      <td>1789</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Fajita Vegetabl...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4506</th>
      <td>1792</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4511</th>
      <td>1794</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Rice, Fajita Veget...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4532</th>
      <td>1802</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Rice, Black Beans,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4537</th>
      <td>1804</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4540</th>
      <td>1805</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Fajita Vegetabl...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4553</th>
      <td>1810</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Black Beans, Sour ...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4555</th>
      <td>1811</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Fajita Vegetables,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4564</th>
      <td>1815</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4574</th>
      <td>1819</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Rice, Cheese, Lett...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4576</th>
      <td>1820</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Cheese, Sour Cream...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4581</th>
      <td>1822</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Rice, Black Beans...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4583</th>
      <td>1823</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Black Bea...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4586</th>
      <td>1824</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4587</th>
      <td>1824</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4589</th>
      <td>1825</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Sour ...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4590</th>
      <td>1825</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Rice, Black Beans,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4591</th>
      <td>1825</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Rice, Black Beans...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4595</th>
      <td>1826</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Rice, Black Bea...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4599</th>
      <td>1827</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Cheese, Lettuce]]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4604</th>
      <td>1828</td>
      <td>1</td>
      <td>Chicken Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>8.75</td>
    </tr>
  </tbody>
</table>
<p>693 rows × 5 columns</p>
</div>



### Step 6. Sort by the name of the item


```python
chipo.item_name.sort_values()

# OR

chipo.sort_values(by = "item_name")
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>order_id</th>
      <th>quantity</th>
      <th>item_name</th>
      <th>choice_description</th>
      <th>item_price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3389</th>
      <td>1360</td>
      <td>2</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>12.98</td>
    </tr>
    <tr>
      <th>341</th>
      <td>148</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1849</th>
      <td>749</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1860</th>
      <td>754</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>2713</th>
      <td>1076</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3422</th>
      <td>1373</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>553</th>
      <td>230</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1916</th>
      <td>774</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1922</th>
      <td>776</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1937</th>
      <td>784</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3836</th>
      <td>1537</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>298</th>
      <td>129</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Sprite]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1976</th>
      <td>798</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1167</th>
      <td>481</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3875</th>
      <td>1554</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1124</th>
      <td>465</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3886</th>
      <td>1558</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>2108</th>
      <td>849</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3010</th>
      <td>1196</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>4535</th>
      <td>1803</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Lemonade]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>4169</th>
      <td>1664</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>4174</th>
      <td>1666</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>4527</th>
      <td>1800</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>4522</th>
      <td>1798</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3806</th>
      <td>1525</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Sprite]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>2389</th>
      <td>949</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3132</th>
      <td>1248</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>3141</th>
      <td>1253</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Lemonade]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>639</th>
      <td>264</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Diet Coke]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>1026</th>
      <td>422</td>
      <td>1</td>
      <td>6 Pack Soft Drink</td>
      <td>[Sprite]</td>
      <td>6.49</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2996</th>
      <td>1192</td>
      <td>1</td>
      <td>Veggie Salad</td>
      <td>[Roasted Chili Corn Salsa (Medium), [Black Bea...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>3163</th>
      <td>1263</td>
      <td>1</td>
      <td>Veggie Salad</td>
      <td>[[Fresh Tomato Salsa (Mild), Roasted Chili Cor...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>4084</th>
      <td>1635</td>
      <td>1</td>
      <td>Veggie Salad</td>
      <td>[[Fresh Tomato Salsa (Mild), Roasted Chili Cor...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>1694</th>
      <td>686</td>
      <td>1</td>
      <td>Veggie Salad</td>
      <td>[[Fresh Tomato Salsa (Mild), Roasted Chili Cor...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>2756</th>
      <td>1094</td>
      <td>1</td>
      <td>Veggie Salad</td>
      <td>[[Tomatillo-Green Chili Salsa (Medium), Roaste...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>4201</th>
      <td>1677</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Black...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>1884</th>
      <td>760</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>455</th>
      <td>195</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>3223</th>
      <td>1289</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Fajita Vegetables...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>2223</th>
      <td>896</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Roasted Chili Corn Salsa, Fajita Vegetables]</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>2269</th>
      <td>913</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>4541</th>
      <td>1805</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Tomatillo Green Chili Salsa, [Fajita Vegetabl...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>3293</th>
      <td>1321</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Black Beans, Chees...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>186</th>
      <td>83</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>960</th>
      <td>394</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Lettu...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>1316</th>
      <td>536</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>2156</th>
      <td>869</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Fajita Vegetables...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4261</th>
      <td>1700</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>295</th>
      <td>128</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Lettu...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4573</th>
      <td>1818</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Pinto...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>2683</th>
      <td>1066</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Roasted Chili Corn Salsa, [Fajita Vegetables,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>496</th>
      <td>207</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Fresh Tomato Salsa, [Rice, Lettuce, Guacamole...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>4109</th>
      <td>1646</td>
      <td>1</td>
      <td>Veggie Salad Bowl</td>
      <td>[Tomatillo Red Chili Salsa, [Fajita Vegetables...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>738</th>
      <td>304</td>
      <td>1</td>
      <td>Veggie Soft Tacos</td>
      <td>[Tomatillo Red Chili Salsa, [Fajita Vegetables...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>3889</th>
      <td>1559</td>
      <td>2</td>
      <td>Veggie Soft Tacos</td>
      <td>[Fresh Tomato Salsa (Mild), [Black Beans, Rice...</td>
      <td>16.98</td>
    </tr>
    <tr>
      <th>2384</th>
      <td>948</td>
      <td>1</td>
      <td>Veggie Soft Tacos</td>
      <td>[Roasted Chili Corn Salsa, [Fajita Vegetables,...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>781</th>
      <td>322</td>
      <td>1</td>
      <td>Veggie Soft Tacos</td>
      <td>[Fresh Tomato Salsa, [Black Beans, Cheese, Sou...</td>
      <td>8.75</td>
    </tr>
    <tr>
      <th>2851</th>
      <td>1132</td>
      <td>1</td>
      <td>Veggie Soft Tacos</td>
      <td>[Roasted Chili Corn Salsa (Medium), [Black Bea...</td>
      <td>8.49</td>
    </tr>
    <tr>
      <th>1699</th>
      <td>688</td>
      <td>1</td>
      <td>Veggie Soft Tacos</td>
      <td>[Fresh Tomato Salsa, [Fajita Vegetables, Rice,...</td>
      <td>11.25</td>
    </tr>
    <tr>
      <th>1395</th>
      <td>567</td>
      <td>1</td>
      <td>Veggie Soft Tacos</td>
      <td>[Fresh Tomato Salsa (Mild), [Pinto Beans, Rice...</td>
      <td>8.49</td>
    </tr>
  </tbody>
</table>
<p>4622 rows × 5 columns</p>
</div>



### Step 7. What was the quantity of the most expensive item ordered?


```python
chipo.sort_values(by = "item_price", ascending = False).head(1)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>order_id</th>
      <th>quantity</th>
      <th>item_name</th>
      <th>choice_description</th>
      <th>item_price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3598</th>
      <td>1443</td>
      <td>15</td>
      <td>Chips and Fresh Tomato Salsa</td>
      <td>NaN</td>
      <td>44.25</td>
    </tr>
  </tbody>
</table>
</div>



### Step 8. How many times were a Veggie Salad Bowl ordered?


```python
chipo_salad = chipo[chipo.item_name == "Veggie Salad Bowl"]

len(chipo_salad)
```




    18



### Step 9. How many times people orderd more than one Canned Soda?


```python
chipo_drink_steak_bowl = chipo[(chipo.item_name == "Canned Soda") & (chipo.quantity > 1)]
len(chipo_drink_steak_bowl)
```




    20


