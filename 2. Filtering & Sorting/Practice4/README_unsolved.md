
# Ex4 - Filtering and Sorting Data

This time we are going to pull data directly from the internet: https://github.com/justmarkham

### Step 1. Import the necessary libraries


### Step 2. Import the dataset from this [address](https://raw.githubusercontent.com/justmarkham/DAT8/master/data/chipotle.tsv). 

### Step 3. Assign it to a variable called chipo.

### Step 4. How many products cost more than $10.00?



### Step 5. What is the price of each item? 
###### print a data frame with only two columns item_name and item_price

### Step 6. Sort by the name of the item


### Step 7. What was the quantity of the most expensive item ordered?



### Step 8. How many times were a Veggie Salad Bowl ordered?

### Step 9. How many times people orderd more than one Canned Soda?