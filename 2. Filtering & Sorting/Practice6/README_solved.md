# EX6 - Fictional Army - Filtering and Sorting

### Introduction:

This exercise was inspired by this [page](http://chrisalbon.com/python/)

Special thanks to: https://github.com/chrisalbon for sharing the dataset and materials.

### Step 1. Import the necessary libraries


```python
import pandas as pd
```

### Step 2. This is the data given as a dictionary


```python
# Create an example dataframe about a fictional army
raw_data = {'regiment': ['Nighthawks', 'Nighthawks', 'Nighthawks', 'Nighthawks', 'Dragoons', 'Dragoons', 'Dragoons', 'Dragoons', 'Scouts', 'Scouts', 'Scouts', 'Scouts'],
            'company': ['1st', '1st', '2nd', '2nd', '1st', '1st', '2nd', '2nd','1st', '1st', '2nd', '2nd'],
            'deaths': [523, 52, 25, 616, 43, 234, 523, 62, 62, 73, 37, 35],
            'battles': [5, 42, 2, 2, 4, 7, 8, 3, 4, 7, 8, 9],
            'size': [1045, 957, 1099, 1400, 1592, 1006, 987, 849, 973, 1005, 1099, 1523],
            'veterans': [1, 5, 62, 26, 73, 37, 949, 48, 48, 435, 63, 345],
            'readiness': [1, 2, 3, 3, 2, 1, 2, 3, 2, 1, 2, 3],
            'armored': [1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1],
            'deserters': [4, 24, 31, 2, 3, 4, 24, 31, 2, 3, 2, 3],
            'origin': ['Arizona', 'California', 'Texas', 'Florida', 'Maine', 'Iowa', 'Alaska', 'Washington', 'Oregon', 'Wyoming', 'Louisana', 'Georgia']}
```

### Step 3. Create a dataframe and assign it to a variable called army. 

#### Don't forget to include the columns names in the order presented in the dictionary ('regiment', 'company', 'deaths'...) so that the column index order is consistent with the solutions. If omitted, pandas will order the columns alphabetically.


```python
army = pd.DataFrame(raw_data, columns = ['regiment', 'company', 'deaths', 'battles', 'size', 'veterans', 'readiness', 'armored', 'deserters', 'origin'])
```

### Step 4. Set the 'origin' colum as the index of the dataframe


```python
army = army.set_index('origin')
army
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>523</td>
      <td>5</td>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>California</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>52</td>
      <td>42</td>
      <td>957</td>
      <td>5</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>25</td>
      <td>2</td>
      <td>1099</td>
      <td>62</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Florida</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>616</td>
      <td>2</td>
      <td>1400</td>
      <td>26</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Maine</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>43</td>
      <td>4</td>
      <td>1592</td>
      <td>73</td>
      <td>2</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Iowa</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>234</td>
      <td>7</td>
      <td>1006</td>
      <td>37</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>523</td>
      <td>8</td>
      <td>987</td>
      <td>949</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Washington</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>62</td>
      <td>3</td>
      <td>849</td>
      <td>48</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Oregon</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>62</td>
      <td>4</td>
      <td>973</td>
      <td>48</td>
      <td>2</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>73</td>
      <td>7</td>
      <td>1005</td>
      <td>435</td>
      <td>1</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Louisana</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>37</td>
      <td>8</td>
      <td>1099</td>
      <td>63</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Georgia</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>35</td>
      <td>9</td>
      <td>1523</td>
      <td>345</td>
      <td>3</td>
      <td>1</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Step 5. Print only the column veterans


```python
army['veterans']
```




    origin
    Arizona         1
    California      5
    Texas          62
    Florida        26
    Maine          73
    Iowa           37
    Alaska        949
    Washington     48
    Oregon         48
    Wyoming       435
    Louisana       63
    Georgia       345
    Name: veterans, dtype: int64



### Step 6. Print the columns 'veterans' and 'deaths'


```python
army[['veterans', 'deaths']]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>veterans</th>
      <th>deaths</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>1</td>
      <td>523</td>
    </tr>
    <tr>
      <th>California</th>
      <td>5</td>
      <td>52</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>62</td>
      <td>25</td>
    </tr>
    <tr>
      <th>Florida</th>
      <td>26</td>
      <td>616</td>
    </tr>
    <tr>
      <th>Maine</th>
      <td>73</td>
      <td>43</td>
    </tr>
    <tr>
      <th>Iowa</th>
      <td>37</td>
      <td>234</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>949</td>
      <td>523</td>
    </tr>
    <tr>
      <th>Washington</th>
      <td>48</td>
      <td>62</td>
    </tr>
    <tr>
      <th>Oregon</th>
      <td>48</td>
      <td>62</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>435</td>
      <td>73</td>
    </tr>
    <tr>
      <th>Louisana</th>
      <td>63</td>
      <td>37</td>
    </tr>
    <tr>
      <th>Georgia</th>
      <td>345</td>
      <td>35</td>
    </tr>
  </tbody>
</table>
</div>



### Step 7. Print the name of all the columns.


```python
army.columns
```




    Index([u'regiment', u'company', u'deaths', u'battles', u'size', u'veterans',
           u'readiness', u'armored', u'deserters'],
          dtype='object')



### Step 8. Select the 'deaths', 'size' and 'deserters' columns from Maine and Alaska


```python
# Select all rows with the index label "Maine" and "Alaska"
army.loc[['Maine','Alaska'] , ["deaths","size","deserters"]]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>deaths</th>
      <th>size</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Maine</th>
      <td>43</td>
      <td>1592</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>523</td>
      <td>987</td>
      <td>24</td>
    </tr>
  </tbody>
</table>
</div>



### Step 9. Select the rows 3 to 7 and the columns 3 to 6


```python
#
army.iloc[3:7, 3:6]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Florida</th>
      <td>2</td>
      <td>1400</td>
      <td>26</td>
    </tr>
    <tr>
      <th>Maine</th>
      <td>4</td>
      <td>1592</td>
      <td>73</td>
    </tr>
    <tr>
      <th>Iowa</th>
      <td>7</td>
      <td>1006</td>
      <td>37</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>8</td>
      <td>987</td>
      <td>949</td>
    </tr>
  </tbody>
</table>
</div>



### Step 10. Select every row after the fourth row


```python
army.iloc[3:]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Florida</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>616</td>
      <td>2</td>
      <td>1400</td>
      <td>26</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Maine</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>43</td>
      <td>4</td>
      <td>1592</td>
      <td>73</td>
      <td>2</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Iowa</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>234</td>
      <td>7</td>
      <td>1006</td>
      <td>37</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>523</td>
      <td>8</td>
      <td>987</td>
      <td>949</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Washington</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>62</td>
      <td>3</td>
      <td>849</td>
      <td>48</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Oregon</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>62</td>
      <td>4</td>
      <td>973</td>
      <td>48</td>
      <td>2</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>73</td>
      <td>7</td>
      <td>1005</td>
      <td>435</td>
      <td>1</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Louisana</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>37</td>
      <td>8</td>
      <td>1099</td>
      <td>63</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Georgia</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>35</td>
      <td>9</td>
      <td>1523</td>
      <td>345</td>
      <td>3</td>
      <td>1</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Step 11. Select every row up to the 4th row


```python
army.iloc[:3]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>523</td>
      <td>5</td>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>California</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>52</td>
      <td>42</td>
      <td>957</td>
      <td>5</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>25</td>
      <td>2</td>
      <td>1099</td>
      <td>62</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
  </tbody>
</table>
</div>



### Step 12. Select the 3rd column up to the 7th column


```python
# the first : means all
# after the comma you select the range

army.iloc[: , 4:7]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>California</th>
      <td>957</td>
      <td>5</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>1099</td>
      <td>62</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Florida</th>
      <td>1400</td>
      <td>26</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Maine</th>
      <td>1592</td>
      <td>73</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Iowa</th>
      <td>1006</td>
      <td>37</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>987</td>
      <td>949</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Washington</th>
      <td>849</td>
      <td>48</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Oregon</th>
      <td>973</td>
      <td>48</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>1005</td>
      <td>435</td>
      <td>1</td>
    </tr>
    <tr>
      <th>Louisana</th>
      <td>1099</td>
      <td>63</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Georgia</th>
      <td>1523</td>
      <td>345</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Step 13. Select rows where df.deaths is greater than 50


```python
army[army['deaths'] > 50]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>523</td>
      <td>5</td>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>California</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>52</td>
      <td>42</td>
      <td>957</td>
      <td>5</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Florida</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>616</td>
      <td>2</td>
      <td>1400</td>
      <td>26</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Iowa</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>234</td>
      <td>7</td>
      <td>1006</td>
      <td>37</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>523</td>
      <td>8</td>
      <td>987</td>
      <td>949</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Washington</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>62</td>
      <td>3</td>
      <td>849</td>
      <td>48</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Oregon</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>62</td>
      <td>4</td>
      <td>973</td>
      <td>48</td>
      <td>2</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>73</td>
      <td>7</td>
      <td>1005</td>
      <td>435</td>
      <td>1</td>
      <td>0</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Step 14. Select rows where df.deaths is greater than 500 or less than 50


```python
army[(army['deaths'] > 500) | (army['deaths'] < 50)]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>523</td>
      <td>5</td>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>25</td>
      <td>2</td>
      <td>1099</td>
      <td>62</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Florida</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>616</td>
      <td>2</td>
      <td>1400</td>
      <td>26</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Maine</th>
      <td>Dragoons</td>
      <td>1st</td>
      <td>43</td>
      <td>4</td>
      <td>1592</td>
      <td>73</td>
      <td>2</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Alaska</th>
      <td>Dragoons</td>
      <td>2nd</td>
      <td>523</td>
      <td>8</td>
      <td>987</td>
      <td>949</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Louisana</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>37</td>
      <td>8</td>
      <td>1099</td>
      <td>63</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Georgia</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>35</td>
      <td>9</td>
      <td>1523</td>
      <td>345</td>
      <td>3</td>
      <td>1</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Step 15. Select all the regiments not named "Dragoons"


```python
army[(army['regiment'] != 'Dragoons')]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>523</td>
      <td>5</td>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>California</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>52</td>
      <td>42</td>
      <td>957</td>
      <td>5</td>
      <td>2</td>
      <td>0</td>
      <td>24</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>25</td>
      <td>2</td>
      <td>1099</td>
      <td>62</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Florida</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>616</td>
      <td>2</td>
      <td>1400</td>
      <td>26</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Oregon</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>62</td>
      <td>4</td>
      <td>973</td>
      <td>48</td>
      <td>2</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Wyoming</th>
      <td>Scouts</td>
      <td>1st</td>
      <td>73</td>
      <td>7</td>
      <td>1005</td>
      <td>435</td>
      <td>1</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>Louisana</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>37</td>
      <td>8</td>
      <td>1099</td>
      <td>63</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>Georgia</th>
      <td>Scouts</td>
      <td>2nd</td>
      <td>35</td>
      <td>9</td>
      <td>1523</td>
      <td>345</td>
      <td>3</td>
      <td>1</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



### Step 16. Select the rows called Texas and Arizona


```python
army.loc[['Arizona', 'Texas']]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>regiment</th>
      <th>company</th>
      <th>deaths</th>
      <th>battles</th>
      <th>size</th>
      <th>veterans</th>
      <th>readiness</th>
      <th>armored</th>
      <th>deserters</th>
    </tr>
    <tr>
      <th>origin</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Arizona</th>
      <td>Nighthawks</td>
      <td>1st</td>
      <td>523</td>
      <td>5</td>
      <td>1045</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>Texas</th>
      <td>Nighthawks</td>
      <td>2nd</td>
      <td>25</td>
      <td>2</td>
      <td>1099</td>
      <td>62</td>
      <td>3</td>
      <td>1</td>
      <td>31</td>
    </tr>
  </tbody>
</table>
</div>



### Step 17. Select the third cell in the row named Arizona


```python
army.loc[['Arizona'], ['deaths']]

#OR

army.iloc[[0], army.columns.get_loc('deaths')]
```




    origin
    Arizona    523
    Name: deaths, dtype: int64



### Step 18. Select the third cell down in the column named deaths


```python
army.loc['Texas', 'deaths']

#OR

army.iloc[[2], army.columns.get_loc('deaths')]
```




    origin
    Texas    25
    Name: deaths, dtype: int64


