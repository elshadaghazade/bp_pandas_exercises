
# Wind Statistics

### Introduction:

The data have been modified to contain some missing values, identified by NaN.  
Using pandas should make this exercise
easier, in particular for the bonus question.

You should be able to perform all of these operations without using
a for loop or other looping construct.


1. The data in 'wind.data' has the following format:


```python
"""
Yr Mo Dy   RPT   VAL   ROS   KIL   SHA   BIR   DUB   CLA   MUL   CLO   BEL   MAL
61  1  1 15.04 14.96 13.17  9.29   NaN  9.87 13.67 10.25 10.83 12.58 18.50 15.04
61  1  2 14.71   NaN 10.83  6.50 12.62  7.67 11.50 10.04  9.79  9.67 17.54 13.83
61  1  3 18.50 16.88 12.33 10.13 11.17  6.17 11.25   NaN  8.50  7.67 12.75 12.71
"""
```




    '\nYr Mo Dy   RPT   VAL   ROS   KIL   SHA   BIR   DUB   CLA   MUL   CLO   BEL   MAL\n61  1  1 15.04 14.96 13.17  9.29   NaN  9.87 13.67 10.25 10.83 12.58 18.50 15.04\n61  1  2 14.71   NaN 10.83  6.50 12.62  7.67 11.50 10.04  9.79  9.67 17.54 13.83\n61  1  3 18.50 16.88 12.33 10.13 11.17  6.17 11.25   NaN  8.50  7.67 12.75 12.71\n'



   The first three columns are year, month and day.  The
   remaining 12 columns are average windspeeds in knots at 12
   locations in Ireland on that day.   

   More information about the dataset go [here](wind.desc).

### Step 1. Import the necessary libraries


```python
import pandas as pd
import datetime
```

### Step 2. Import the dataset from this [address](https://github.com/guipsamora/pandas_exercises/blob/master/06_Stats/Wind_Stats/wind.data)

### Step 3. Assign it to a variable called data and replace the first 3 columns by a proper datetime index.


```python
# parse_dates gets 0, 1, 2 columns and parses them as the index
data_url = 'https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/06_Stats/Wind_Stats/wind.data'
data = pd.read_table(data_url, sep = "\s+", parse_dates = [[0,1,2]]) 
data.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Yr_Mo_Dy</th>
      <th>RPT</th>
      <th>VAL</th>
      <th>ROS</th>
      <th>KIL</th>
      <th>SHA</th>
      <th>BIR</th>
      <th>DUB</th>
      <th>CLA</th>
      <th>MUL</th>
      <th>CLO</th>
      <th>BEL</th>
      <th>MAL</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2061-01-01</td>
      <td>15.04</td>
      <td>14.96</td>
      <td>13.17</td>
      <td>9.29</td>
      <td>NaN</td>
      <td>9.87</td>
      <td>13.67</td>
      <td>10.25</td>
      <td>10.83</td>
      <td>12.58</td>
      <td>18.50</td>
      <td>15.04</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2061-01-02</td>
      <td>14.71</td>
      <td>NaN</td>
      <td>10.83</td>
      <td>6.50</td>
      <td>12.62</td>
      <td>7.67</td>
      <td>11.50</td>
      <td>10.04</td>
      <td>9.79</td>
      <td>9.67</td>
      <td>17.54</td>
      <td>13.83</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2061-01-03</td>
      <td>18.50</td>
      <td>16.88</td>
      <td>12.33</td>
      <td>10.13</td>
      <td>11.17</td>
      <td>6.17</td>
      <td>11.25</td>
      <td>NaN</td>
      <td>8.50</td>
      <td>7.67</td>
      <td>12.75</td>
      <td>12.71</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2061-01-04</td>
      <td>10.58</td>
      <td>6.63</td>
      <td>11.75</td>
      <td>4.58</td>
      <td>4.54</td>
      <td>2.88</td>
      <td>8.63</td>
      <td>1.79</td>
      <td>5.83</td>
      <td>5.88</td>
      <td>5.46</td>
      <td>10.88</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2061-01-05</td>
      <td>13.33</td>
      <td>13.25</td>
      <td>11.42</td>
      <td>6.17</td>
      <td>10.71</td>
      <td>8.21</td>
      <td>11.92</td>
      <td>6.54</td>
      <td>10.92</td>
      <td>10.34</td>
      <td>12.92</td>
      <td>11.83</td>
    </tr>
  </tbody>
</table>
</div>



### Step 4. Year 2061? Do we really have data from this year? Create a function to fix it and apply it.

### Step 5. Set the right dates as the index. Pay attention at the data type, it should be datetime64[ns].

### Step 6. Compute how many values are missing for each location over the entire record.  
#### They should be ignored in all calculations below. 


### Step 7. Compute how many non-missing values there are in total.

### Step 8. Calculate the mean windspeeds of the windspeeds over all the locations and all the times.
#### A single number for the entire dataset.


### Step 9. Create a DataFrame called loc_stats and calculate the min, max and mean windspeeds and standard deviations of the windspeeds at each location over all the days 

#### A different set of numbers for each location.


### Step 10. Create a DataFrame called day_stats and calculate the min, max and mean windspeed and standard deviations of the windspeeds across all the locations at each day.

#### A different set of numbers for each day.

### Step 11. Find the average windspeed in January for each location.  
#### Treat January 1961 and January 1962 both as January.

### Step 12. Downsample the record to a yearly frequency for each location.

### Step 13. Downsample the record to a monthly frequency for each location.

### Step 14. Downsample the record to a weekly frequency for each location.

### Step 15. Calculate the min, max and mean windspeeds and standard deviations of the windspeeds across all locations for each week (assume that the first week starts on January 2 1961) for the first 52 weeks.