
# US - Baby Names

### Introduction:

We are going to use a subset of [US Baby Names](https://www.kaggle.com/kaggle/us-baby-names) from Kaggle.  
In the file it will be names from 2004 until 2014


### Step 1. Import the necessary libraries


```python
import pandas as pd
```

### Step 2. Import the dataset from this [address](https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/06_Stats/US_Baby_Names/US_Baby_Names_right.csv). 

### Step 3. Assign it to a variable called baby_names.


```python
baby_names = pd.read_csv('https://raw.githubusercontent.com/guipsamora/pandas_exercises/master/06_Stats/US_Baby_Names/US_Baby_Names_right.csv')
baby_names.info()
```

### Step 4. See the first 10 entries


```python
baby_names.head(10)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Unnamed: 0</th>
      <th>Id</th>
      <th>Name</th>
      <th>Year</th>
      <th>Gender</th>
      <th>State</th>
      <th>Count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>11349</td>
      <td>11350</td>
      <td>Emma</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>62</td>
    </tr>
    <tr>
      <th>1</th>
      <td>11350</td>
      <td>11351</td>
      <td>Madison</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>48</td>
    </tr>
    <tr>
      <th>2</th>
      <td>11351</td>
      <td>11352</td>
      <td>Hannah</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>46</td>
    </tr>
    <tr>
      <th>3</th>
      <td>11352</td>
      <td>11353</td>
      <td>Grace</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>44</td>
    </tr>
    <tr>
      <th>4</th>
      <td>11353</td>
      <td>11354</td>
      <td>Emily</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>41</td>
    </tr>
    <tr>
      <th>5</th>
      <td>11354</td>
      <td>11355</td>
      <td>Abigail</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>37</td>
    </tr>
    <tr>
      <th>6</th>
      <td>11355</td>
      <td>11356</td>
      <td>Olivia</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>33</td>
    </tr>
    <tr>
      <th>7</th>
      <td>11356</td>
      <td>11357</td>
      <td>Isabella</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>30</td>
    </tr>
    <tr>
      <th>8</th>
      <td>11357</td>
      <td>11358</td>
      <td>Alyssa</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>29</td>
    </tr>
    <tr>
      <th>9</th>
      <td>11358</td>
      <td>11359</td>
      <td>Sophia</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>28</td>
    </tr>
  </tbody>
</table>
</div>



### Step 5. Delete the column 'Unnamed: 0' and 'Id'


```python
# deletes Unnamed: 0
del baby_names['Unnamed: 0']

# deletes Unnamed: 0
del baby_names['Id']

baby_names.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Name</th>
      <th>Year</th>
      <th>Gender</th>
      <th>State</th>
      <th>Count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Emma</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>62</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Madison</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>48</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Hannah</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>46</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Grace</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>44</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Emily</td>
      <td>2004</td>
      <td>F</td>
      <td>AK</td>
      <td>41</td>
    </tr>
  </tbody>
</table>
</div>



### Step 6. Are there more male or female names in the dataset?


```python
baby_names['Gender'].value_counts()
```




    F    558846
    M    457549
    Name: Gender, dtype: int64



### Step 7. Group the dataset by name and assign to names


```python
# you don't want to sum the Year column, so you delete it
del baby_names["Year"]

# group the data
names = baby_names.groupby("Name").sum()

# print the first 5 observations
names.head()

# print the size of the dataset
print(names.shape)

# sort it from the biggest value to the smallest one
names.sort_values("Count", ascending = 0).head()
```

    (17632, 1)





<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Count</th>
    </tr>
    <tr>
      <th>Name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Jacob</th>
      <td>242874</td>
    </tr>
    <tr>
      <th>Emma</th>
      <td>214852</td>
    </tr>
    <tr>
      <th>Michael</th>
      <td>214405</td>
    </tr>
    <tr>
      <th>Ethan</th>
      <td>209277</td>
    </tr>
    <tr>
      <th>Isabella</th>
      <td>204798</td>
    </tr>
  </tbody>
</table>
</div>



### Step 8. How many different names exist in the dataset?


```python
# as we have already grouped by the name, all the names are unique already. 
# get the length of names
len(names)
```




    17632



### Step 9. What is the name with most occurrences?


```python
names.Count.idxmax()

# OR

# names[names.Count == names.Count.max()]
```




    'Jacob'



### Step 10. How many different names have the least occurrences?


```python
len(names[names.Count == names.Count.min()])
```




    2578



### Step 11. What is the median name occurrence?


```python
names[names.Count == names.Count.median()]
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Count</th>
    </tr>
    <tr>
      <th>Name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Aishani</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Alara</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Alysse</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Ameir</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Anely</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Antonina</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Aveline</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Aziah</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Baily</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Caleah</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Carlota</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Cristine</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Dahlila</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Darvin</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Deante</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Deserae</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Devean</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Elizah</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Emmaly</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Emmanuela</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Envy</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Esli</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Fay</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Gurshaan</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Hareem</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Iven</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Jaice</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Jaiyana</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Jamiracle</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Jelissa</th>
      <td>49</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>Kyndle</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Kynsley</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Leylanie</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Maisha</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Malillany</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Mariann</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Marquell</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Maurilio</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Mckynzie</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Mehdi</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Nabeel</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Nalleli</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Nassir</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Nazier</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Nishant</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Rebecka</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Reghan</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Ridwan</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Riot</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Rubin</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Ryatt</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Sameera</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Sanjuanita</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Shalyn</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Skylie</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Sriram</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Trinton</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Vita</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Yoni</th>
      <td>49</td>
    </tr>
    <tr>
      <th>Zuleima</th>
      <td>49</td>
    </tr>
  </tbody>
</table>
<p>66 rows × 1 columns</p>
</div>



### Step 12. What is the standard deviation of names?


```python
names.Count.std()
```




    11006.069467891111



### Step 13. Get a summary with the mean, min, max, std and quartiles.


```python
names.describe()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>17632.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>2008.932169</td>
    </tr>
    <tr>
      <th>std</th>
      <td>11006.069468</td>
    </tr>
    <tr>
      <th>min</th>
      <td>5.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>11.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>49.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>337.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>242874.000000</td>
    </tr>
  </tbody>
</table>
</div>


